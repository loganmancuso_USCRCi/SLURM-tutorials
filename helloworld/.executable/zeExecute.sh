#!/bin/bash
###
# 'zeExecute.sh'
# this program will execute the Aprog in the test directory
#
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 11-29-2017--11:04:36
###

IO_FILES="../../iofiles"
X_IN="$IO_FILES/x.in"
X_OUT="$IO_FILES/x.out"
X_LOG="$IO_FILES/x.log"


cd ../../iofiles 
rm *.out *.log
cd ../.test/src
echo "EXECUTING"
python aprog.py $X_IN $X_OUT $X_LOG
echo "EXECUTION COMPLETE"

###
# End 'zeExecute.sh'
###