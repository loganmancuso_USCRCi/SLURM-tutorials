'''
	'aprog.py'
	Author/CopyRight: Mancuso, Logan
	Last Edit Date: 06-12-2019--12:57:24
'''

# Packages
import sys
import os
import cv2 as cv
import numpy as np

### main function 
def main(argv):
	image = cv.imread( argv[1] )
	print(image)

### main function call
if __name__ == "__main__":
	main(sys.argv) #execute main function

'''
	End 'aprog.py'
'''
