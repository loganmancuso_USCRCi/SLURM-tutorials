#!/bin/bash
###
# 'submit.sh'
# This script will run the program, run from
# .test/src/
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 05-09-2019--17:03:36
###

### SLURM
#SBATCH --job-name=example05
#SBATCH --partition=no-kill
#SBATCH --nodes=1
#SBATCH --ntasks-per-socket=1
#SBATCH --ntasks-per-node=1
#SBATCH --error=ERROR_%j.err
#SBATCH --output=OUTPUT_%j.out
#

### Modules
module load opencv

### Variables
SH_LOG="../../iofiles/sh.log"
X_IN="../../iofiles/image.jpg"
X_LOG="../../iofiles/x.log"

### Begin
cd iofiles #remove old files
rm *.out *.err *.log &>> sh.log
cd ../.test/src/ #change to dir that has program
echo "Program Begining Execution" >> $SH_LOG
python aprog.py $X_IN  #run program
echo "Program Done Executing" >> $SH_LOG

### Cleanup
cd ../../ && mv *.{out,err} iofiles/ &>>iofiles/sh.log #move log and error files to io directory
# remove modules here
module unload opencv

###
# End 'submit.sh'
###
