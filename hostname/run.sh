#!bin/bash

###
# 'run.sh'
# This script will run the program, run from
# .TestDirectory/src/
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 05-01-2019--16:51:18
###


###
# Error Messages
###

WRONG_DIRECTORY="Wrong Directory, Move To Working Directory To Run Program"
ZE="../../.executable/zeExecute.sh"
SH_LOG="../../iofiles/sh.log"

if [[ ! $PWD/ = */working/src/ ]]; then
  cd working/src
	if [[ ! $PWD/ = */working/src/ ]]; then
		echo $WRONG_DIRECTORY
		exit 1
  fi
fi

echo "Executing zeExecute.sh" >> $SH_LOG
echo "-----------------------------------------------------------------" >> $SH_LOG
./$ZE
echo "Program Done Executing"

###
# End 'run.sh'
###