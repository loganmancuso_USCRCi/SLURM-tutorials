/**
 * 'main.c'
 * Main file for programming in c
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 05-01-2019--16:51:18
 *
**/

/**
 * included files/library
**/
#include <stdio.h>
#include <stdlib.h>

/**
 * global variables
**/

/**
 * Function: 'Main'
**/

int main(int argc, char *argv[]) {

  /**
   * x.in = argv 1
   * x.out = argv 2
   * x.log = argv 3
  **/

  char const* const in_file = argv[1];
  char const* const out_file = argv[2];
  char const* const log_file = argv[3];
  FILE *file = fopen(in_file, "r");
  printf("Reading From File %s\n",in_file);
  char line[256];
  int i = 0;
  while(fgets(line, sizeof(line), file)) {
    printf("Line Read: %d\n",atoi(line));
  }

  printf("Beginning Computation\n");

  /**
    * add your code here
  **/

  printf("Main Function Complete\n");

  return 0;

}//end main

/**
 * End 'main.c'
**/