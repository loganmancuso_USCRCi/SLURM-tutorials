#!/bin/bash
###
# 'zcCopyProgram.sh'
# this program will untar all files in 'backup'
# storing it to the 'test' for testing
#
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 11-29-2017--11:04:36
###

cd ../../
echo "Remove the old 'test' directory"
rm -r .test
echo "Create a new 'test' directory"
mkdir .test
echo "cp files from 'backup' into 'test'"
cp .backup/project.tar.gz .test
#
echo "Descend into 'test' directory"
cd .test/
tar -xvf project.tar.gz
rm project.tar.gz
cd src/
for item in *
do
  sed -i "/.* Last Edit Date:*/a\ * THIS IS A TEST FILE CHANGES WILL NOT BE SAVED" $item
done
cd ../

echo "Return To 'working' directory"
cd ../working/src

###
# End 'zcCopyProgram.sh'
###