#!/bin/bash
###
# 'zeExecute.sh'
# this program will execute the Aprog in the test directory
#
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 11-29-2017--11:04:36
###

cd ../../iofiles 
rm x.out x.log
cd ../.test/src
include=../../iofiles
echo "EXECUTING"
./Aprog $include/x.in $include/x.out $include/x.log
echo "EXECUTION COMPLETE"
cd ../../working/src

###
# End 'zeExecute.sh'
###
