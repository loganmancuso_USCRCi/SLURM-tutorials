#!/bin/bash
###
# 'zdCompile.sh'
# this program will compile the files in the test directory
#
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 11-29-2017--11:04:36
###

echo "Descend into 'test' directory"
cd ../../.test/src

echo "COMPILING" $item
make -f ../makefile clean
make -f ../makefile all
if [ $? -eq 0 ]; then
	echo "Build Complete"
else
	echo "Build Failed"
fi

echo "Return To 'working' directory"
cd ../../working/src/
echo " "
echo "COMPILING COMPLETE"
echo " "

###
# End 'zdCompile.sh'
###