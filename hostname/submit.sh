#!/bin/bash
#
#SBATCH --export=NONE
#SBATCH --job-name=hostname
#SBATCH --output=x.out
#
#SBATCH --ntask-per-node=1
#SBATCH --time=10:00
#SBATCH --mem-per-cpu=100

srun hostname

