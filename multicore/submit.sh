#!/bin/bash
###
# 'submit.sh'
# This script will run the program, run from
# .TestDirectory/src/
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 05-09-2019--17:03:36
###

### SLURM
#SBATCH --job-name=multicore
#SBATCH --partition=no-kill
#SBATCH --nodes=1
#SBATCH --ntasks-per-socket=10
#SBATCH --ntasks-per-node=10
#SBATCH --error=ERROR_%j.err
#SBATCH --output=OUTPUT_%j.out
#

### Modules
module load openmpi

### Variables
SH_LOG="../../iofiles/sh.log"
X_IN="../../iofiles/x.in"
X_LOG="../../iofiles/x.log"

### Begin
cd iofiles #remove old files
rm *.out *.err *.log &>> sh.log
cd ../.test/src/ #change to dir that has program
echo "Program Begining Execution" >> $SH_LOG
for THREAD in $(seq 1 $SLURM_NTASKS_PER_NODE); do
	python aprog.py $THREAD  #run program
done
echo "Program Done Executing" >> $SH_LOG
cd ../../ && mv *.{out,err} iofiles/ &>>iofiles/sh.log #move log and error files to io directory


###
# End 'submit.sh'
###
