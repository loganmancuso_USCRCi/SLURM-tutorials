'''
	'aprog.py'
	Author/CopyRight: Mancuso, Logan
	Last Edit Date: 06-12-2019--12:22:38
'''

# Packages
import sys
import os

### main function 
def main(argv):
	print(int(argv[1]) ** 2)


### main function call
if __name__ == "__main__":
	main(sys.argv) #execute main function

'''
	End 'aprog.py'
'''
