#!/bin/bash
###
# 'submit.sh'
# This script will run the program, run from
# .TestDirectory/src/
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 05-09-2019--17:03:36
###

### SLURM
#SBATCH --job-name=Example03
#SBATCH --ntasks-per-node=1
#SBATCH -N 1
#SBATCH -e %j.err
#SBATCH -o %j.out
#

### Modules

### Variables
SH_LOG="../../iofiles/sh.log"
X_IN="../../iofiles/x.in"
X_LOG="../../iofiles/x.log"

### Begin
cd iofiles #remove old files
rm *.out *.err *.log &>> sh.log
cd ../.test/src/ #change to dir that has program
echo "Program Begining Execution" >> $SH_LOG
python aprog.py $X_IN $X_LOG #run program
echo "Program Done Executing" >> $SH_LOG
cd ../../ && mv *.{out,err} iofiles/ #move log and error files to io directory

###
# End 'submit.sh'
###
