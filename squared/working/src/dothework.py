'''
	'dothework.py'
	Author/CopyRight: Mancuso, Logan
	Last Edit Date: 06-12-2019--11:29:22
'''

# Packages
import sys
import os

### File I/O function
def write_out( log_file, out_stream ):
	with open(log_file,"a+") as text_file:
		text_file.write(out_stream + "\n")
		text_file.close()

### initialize the computation by reading from the file
def __init__(input_array, log_file):
	# input array is an array comprised of each line from the input file
	for a_line in input_array:
		print( "Read This Line: " + a_line )
		write_out( log_file,"Read This: " + a_line )
		compute(a_line)

# compute
def compute( a_number ):
	final = int(a_number) ** 2
	print( "Squared = " + str(final) )
	return 0

'''
	End 'dothework.py'
'''
